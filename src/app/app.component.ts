import {Component, OnInit} from '@angular/core';
import {AuthService} from "./service/login-service/AuthService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  isAuth = true;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.auth.subscribe(value => this.isAuth = value != undefined);
  }
}
