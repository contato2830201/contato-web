import { Injectable } from '@angular/core';
import {ApiRequestService} from "../util/api-request.service";
import {HttpParams} from "@angular/common/http";
import {Contato} from "../model/Contato.model";
import {Page, Paginator} from "../model/Page.model";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private apiRequestService: ApiRequestService) { }

  findById(id:number){
    return this.apiRequestService.get<Contato>(`contato/${id}`);
  }

  findByValueAndId(value:string, id?:number){
    let params = new HttpParams().append('valor', value);
    if(id != null)
      params = params.append("id", id)
    return this.apiRequestService.get<void>(`contato/valida-campo`, {params: params});
  }

  findAll(name:string, favorite?: boolean,  page?:Paginator){
    let params = new HttpParams().append('name', name);
    if(page?.getPage != null && page?.getSize){
      params = params.append("size", page?.getSize);
      params = params.append("page", page?.getPage);
    }
    if(favorite != null)
      params = params.append("favorite", favorite)
    return this.apiRequestService.get<Page<Contato>>(`contato`, {params: params});
  }

  toogleFavorite(id: number){
    return this.apiRequestService.post<void>(`contato/favorite/${id}`);
  }

  toogleDisable(id: number){
    return this.apiRequestService.post<void>(`contato/disabled/${id}`);
  }

  save(contato:Contato){
    return this.apiRequestService.post<Contato>(`contato`, contato);
  }

  edit(contato:Contato){
    return this.apiRequestService.put<Contato>(`contato`, contato);
  }
}
