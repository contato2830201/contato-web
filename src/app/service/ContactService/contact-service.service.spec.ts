import { TestBed } from '@angular/core/testing';

import { ContactService } from './ContactService.service';

describe('ContactServiceService', () => {
  let service: ContactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
