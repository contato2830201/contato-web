export class Contato {
  id?: number;
  name?: string;
  mail?: null | undefined | string;
  cellPhone?: null | undefined | string;
  homePhone?: null | undefined | string;
  favorite?: boolean;
  active?: boolean;

}
