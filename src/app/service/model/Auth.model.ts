export interface Auth {
  token: string;
  fullName: string;
}
