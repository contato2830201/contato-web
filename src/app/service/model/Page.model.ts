export class Page<T> {
  content?: T[];
  pageable?: Pageable;
  totalPages?: number;
  totalElements?: number;
  last?: boolean;
  size?: number;
  number?: number;
  sort?: Sort;
  first?: boolean;
  numberOfElements?: number;
  empty?: boolean;
}

export class Pageable {
  sort?: Sort;
  offset?: number;
  pageNumber?: number;
  pageSize?: number;
  paged?: boolean;
  unpaged?: boolean;
}

export interface Sort {
  sorted?: boolean;
  unsorted?: boolean;
  empty?: boolean;
}

export class Paginator {
  private page: number = 0;
  private size: number = 10;
  private sort: string = "";

  of(page: number, size: number, sort: string = ""): Paginator {
    this.page = page;
    this.size = size;
    this.sort = sort
    return this;
  }

  public get getPage(): number {
    return this.page;
  }
  public get getSize(): number {
    return this.size;
  }
  public get getSort(): String {
    return this.sort;
  }
}
