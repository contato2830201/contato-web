import {Injectable} from '@angular/core';
import {ApiRequestService} from "../util/api-request.service";
import {MessageService} from "primeng/api";
import {User} from "../model/User.model";
import {map} from "rxjs/operators";
import {BehaviorSubject, Observable} from "rxjs";
import {Auth} from "../model/Auth.model";
import {Router} from "@angular/router";
import {Contato} from "../model/Contato.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authSubject: BehaviorSubject<Auth | null>;
  public auth: Observable<Auth | null>;

  constructor(private apiRequestService: ApiRequestService, private messageService: MessageService,
  private router: Router) {
    this.authSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('auth')!));
    this.auth = this.authSubject.asObservable();
  }

  public get userValue() {
    return this.authSubject.value;
  }

  login(user: User) {
    return this.apiRequestService.post<Auth>(`/rest/auth/login`, user)
      .pipe(map(auth => {
        localStorage.setItem('auth', JSON.stringify(auth));
        this.authSubject.next(auth);
        return auth;
      }));
  }

  logout(){
    // remove user from local storage and set current user to null
    localStorage.removeItem('auth');
    this.authSubject.next(null);
    this.router.navigate(['/login']);
  }

}
