import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import {ContatoViewComponent} from "../../page/contato/contato-view/contato-view.component";
import {ConfirmationDialogComponent} from "../../util/confirmation-dialog/confirmation-dialog.component";
import {first} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ConfirmDeactivateGuard implements CanDeactivate<any> {
    canDeactivate(target: ContatoViewComponent) {
        if (target.validDataByRefresh()) {
          return target.dialog.open(ConfirmationDialogComponent, {
            width: '500px',
            data: {
              title: "Atenção",
              message: "Deseja relamente sair, você perderá as alterações?"
            }
          }).afterClosed().pipe(map(value => !!value));
        }
        return true;
    }
}
