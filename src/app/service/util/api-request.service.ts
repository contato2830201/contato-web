import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export abstract class ApiRequestService {

  private API: String = environment.BASE_API;

  constructor(private httpClient:HttpClient) { }

  get<T>(
    url: string,
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: "body";
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: "json";
      withCredentials?: boolean;
    }
  ): Observable<T> {
    return this.httpClient
      .get<T>(`${this.API}/${url}`, options);
  }
  post<T>(
    url: string,
    body?: any | {},
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: "body";
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: "json";
      withCredentials?: boolean;
    }
  ): Observable<T> {
    return this.httpClient
      .post<T>(`${this.API}/${url}`, body, options);
  }

  put<T>(
    url: string,
    body?: any | {},
    options?: {
      headers?:
        | HttpHeaders
        | {
        [header: string]: string | string[];
      };
      observe?: "body";
      params?:
        | HttpParams
        | {
        [param: string]: string | string[];
      };
      reportProgress?: boolean;
      responseType?: "json";
      withCredentials?: boolean;
    }
  ): Observable<T> {
    return this.httpClient
      .put<T>(`${this.API}/${url}`, body, options);
  }


}
