import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError, } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from "../../login-service/AuthService";
import {MessageService} from "primeng/api";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private accountService: AuthService, private messageService:MessageService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if ([401, 403].includes(err.status) && this.accountService.userValue) {
        this.accountService.logout();
      }else if ([404, 422, 409].includes(err.status)) {
        this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: err.error.message });
      }else{
        const error = err.error?.message || err.statusText;
        this.messageService.add({ severity: 'severity', summary: 'Erro', detail: error });
      }
      return throwError(() => err);
    }))
  }
}
