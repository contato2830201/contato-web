import { NgModule } from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./page/login/login.component";
import {ContatoListaComponent} from "./page/contato/contato-lista/contato-lista.component";
import {ContatoViewComponent} from "./page/contato/contato-view/contato-view.component";
import {AuthGuard} from "./service/util/auth-guard-service/auth-guard.service";
import {AppComponent} from "./app.component";
import {ConfirmDeactivateGuard} from "./service/util/ConfirmDeactivateGuard";

const routes: Routes = [
    {
      path: '',
      redirectTo: '/contato',
      pathMatch: "full"

    },
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "contato",
        component: ContatoListaComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "contato/edit/:id",
        component: ContatoViewComponent,
        canActivate: [AuthGuard],
        canDeactivate: [ConfirmDeactivateGuard]
      },
      {
        path: "contato/save",
        component: ContatoViewComponent,
        canActivate: [AuthGuard],
        canDeactivate: [ConfirmDeactivateGuard]
      }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
