
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './page/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from "./page/header/header.component";
import {ContatoListaComponent} from './page/contato/contato-lista/contato-lista.component';
import {CommonModule, NgStyle, registerLocaleData} from "@angular/common";
import localePT from '@angular/common/locales/pt';
import {ContatoViewComponent} from './page/contato/contato-view/contato-view.component';
import {ConfirmationDialogComponent} from './util/confirmation-dialog/confirmation-dialog.component';
import {MessageService} from "primeng/api";
import {ToastModule} from "primeng/toast";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorIntl, MatPaginatorModule} from "@angular/material/paginator";
import {RouterLink} from "@angular/router";
import {LOCALE_ID, NgModule} from "@angular/core";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ErrorInterceptor} from "./service/util/inteceptor/ErrorInterceptor";
import {TokenInterceptor} from "./service/util/inteceptor/TokenInterceptor";
import {NgxMaskDirective, NgxMaskModule, NgxMaskPipe, NgxMaskService} from "ngx-mask";
import {ConfirmDeactivateGuard} from "./service/util/ConfirmDeactivateGuard";
import {getPortuguesePaginatorIntl} from "./util/PaginatorI18n";

registerLocaleData(localePT);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HeaderComponent,
    ContatoViewComponent,
    ConfirmationDialogComponent,
    ToastModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    MatGridListModule, MatCheckboxModule, MatTableModule, MatPaginatorModule, RouterLink, NgStyle,
    ContatoListaComponent,
    NgxMaskModule.forRoot({})
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pt-BR',
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    {
      provide: MatPaginatorIntl,
      useValue: getPortuguesePaginatorIntl()
    },
    MessageService
  ],
  bootstrap: [AppComponent],
  exports:[
    CommonModule,
    BrowserModule,
    HttpClientModule
  ]
})
export class AppModule { }
