import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {MessageService} from 'primeng/api';
import {User} from "../../service/model/User.model";
import {first} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../service/login-service/AuthService";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hide = true;
  submitted = false;

  formLogin = this.fb.group({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  })

  constructor(private authService: AuthService, private fb: FormBuilder, private messageService: MessageService,
  private router: Router,
  private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.authService.auth.subscribe(value => {
      if (value != null){
        this.redirectLogin();
      }
    });
  }

  get f() { return this.formLogin.controls; }

  save() {
    if(this.formLogin.invalid){
      this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: "Formulário invalido, favor corrigir!" });
      return;
    }
    this.submitted = true;

    this.authService.login(<User> this.formLogin.value)
      .pipe(first())
        .subscribe({
          next: () => {
            this.redirectLogin();
          }
        });
  }

  private redirectLogin() {
    const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.router.navigateByUrl(returnUrl);
  }
}
