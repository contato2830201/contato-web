import {Component, OnInit} from '@angular/core';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatChipsModule} from "@angular/material/chips";
import {MatMenuModule} from "@angular/material/menu";
import {AuthService} from "../../service/login-service/AuthService";
import {ConfirmationDialogComponent} from "../../util/confirmation-dialog/confirmation-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  standalone: true,
  imports: [MatToolbarModule, MatButtonModule, MatIconModule, MatChipsModule, MatMenuModule],
})
export class HeaderComponent implements OnInit {
  username?:string = "";



  constructor(private authService: AuthService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.authService.auth.subscribe(next=> {
      this.username = next?.fullName;
    });
  }

  close = ()=> this.dialog.open(ConfirmationDialogComponent, {
    width: '500px',
    data: {
      title: "Atenção",
      message: "Deseja relamente deletar esse registro?"
    }
  }).afterClosed().subscribe(response => {
    if(response)
      this.authService.logout();
  })

}
