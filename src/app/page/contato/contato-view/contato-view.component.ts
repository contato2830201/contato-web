import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatButtonModule} from "@angular/material/button";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatInputModule} from "@angular/material/input";
import {Location, NgIf} from "@angular/common";
import {NgxMaskModule} from "ngx-mask";
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  ValidationErrors, ValidatorFn,
  Validators
} from "@angular/forms";
import {Contato} from "../../../service/model/Contato.model";
import {ContactService} from "../../../service/ContactService/ContactService.service";
import {MessageService} from "primeng/api";
import {ActivatedRoute, Router} from "@angular/router";
import {debounceTime, first, Observable, of} from "rxjs";

import {catchError, map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationDialogComponent} from "../../../util/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'app-contato-view',
  templateUrl: './contato-view.component.html',
  styleUrls: ['./contato-view.component.scss'],
  standalone: true,
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatGridListModule,
    MatInputModule,
    NgxMaskModule,
    ReactiveFormsModule,
    NgIf,
  ]
})
export class ContatoViewComponent implements OnInit {
  id?:number;
  submitted:boolean = false;
  valueOriginal?: Contato;
  formContato = this.fb.group({
    "id": new FormControl<number | null>(null),
    "name": new FormControl("", Validators.required),
    "mail": new FormControl("", {validators: [Validators.email], asyncValidators:this.validatorDuplicateData(), updateOn: "blur"}),
    "cellPhone": new FormControl("", {validators: [Validators.required], asyncValidators:this.validatorDuplicateData(), updateOn: "blur"}),
    "homePhone": new FormControl("", {asyncValidators:this.validatorDuplicateData(), updateOn: "blur"}),
    "favorite": new FormControl(false),
    "active": new FormControl(true)
  });

  constructor(private location: Location, private contatoService: ContactService, private fb: FormBuilder, public messageService: MessageService,
              public dialog:MatDialog,
              private route: ActivatedRoute,
              private router: Router) { }


  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    if(this.id != null)
      this.findById(this.id);
  }
  get f() { return this.formContato.controls; }
  save = ()=> {
    if(this.formContato.invalid){
      this.messageService.add({severity: 'warn', summary: 'Atenção', detail: `Existem campos invalidos no formulário`});
      return;
    }
    let contato:Contato = <Contato>this.formContato.getRawValue();
    contato.mail = contato.mail != undefined && contato.mail.length == 0 ? null : contato.mail;
    contato.cellPhone = contato.cellPhone != undefined && contato.cellPhone.length == 0 ? null : contato.cellPhone;
    contato.homePhone = contato.homePhone != undefined && contato.homePhone.length == 0 ? null : contato.homePhone;
    this.saveContato(contato).subscribe(value => {
      this.messageService.add({severity: 'success', summary: 'Sucesso', detail: `Contato salvo com sucesso!`});
      this.router.navigate(["/contato/edit", value.id]);
      this.valueOriginal = <Contato> this.formContato.getRawValue();
      this.id = value.id;
    });
  }

  saveContato = (contato: Contato)=>
                    !this.id ?
                      this.contatoService.save(contato) :
                      this.contatoService.edit(contato)


  back = (e:Event) => {
    e.stopPropagation();
    e.preventDefault();
    this.router.navigate(["/contato"]);
  }

  findById = (id:number)=> this.contatoService.findById(id!).pipe(first()).subscribe(value => {
    this.valueOriginal = value;
    this.formContato.patchValue(value);
  })

  validatorDuplicateData(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      if (control.value != null && control.value.length > 0)
        return this.contatoService.findByValueAndId(control.value, this.id)
                                              .pipe(
                                                debounceTime(500),
                                                map(() => null),
                                                catchError((err) => of({conflitData: true}
                                              )));
      return of(null);
    };
  }

  validDataByRefresh = ()=> (this.id == undefined && this.formContato.touched) || this.valueOriginal != undefined && JSON.stringify(this.valueOriginal) !== JSON.stringify(this.formContato.getRawValue());

}
