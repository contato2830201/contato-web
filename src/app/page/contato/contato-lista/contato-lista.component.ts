import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationDialogComponent} from "../../../util/confirmation-dialog/confirmation-dialog.component";
import {FormBuilder, FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ContactService} from "../../../service/ContactService/ContactService.service";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule, PageEvent} from "@angular/material/paginator";
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {Page, Paginator} from "../../../service/model/Page.model";
import {Contato} from "../../../service/model/Contato.model";
import {MessageService} from "primeng/api";
import {MatSelectModule} from "@angular/material/select";
import {NgxMaskDirective, NgxMaskModule, NgxMaskPipe} from "ngx-mask";
import {InputMaskModule} from "primeng/inputmask";
import {AppModule} from "../../../app.module";

@Component({
  selector: 'app-contato-lista',
  templateUrl: './contato-lista.component.html',
  styleUrls: ['./contato-lista.component.scss'],
  standalone:true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatPaginatorModule,
    RouterModule,
    MatTableModule,
    MatGridListModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    NgxMaskModule
  ]
})
export class ContatoListaComponent implements OnInit {

  dataSource: Page<Contato> = {};
  paginator: Paginator = new Paginator().of(0, 10);

  formSearch = this.fb.group({
    name: new FormControl(""),
    favorite: new FormControl(null),
  })

  displayedColumns = ['name','mail','cellPhone','homePhone', "action"];

  constructor(public dialog: MatDialog, private fb: FormBuilder, private contactService :ContactService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.search(0, 10);
  }

  search(page:number, size:number){
    this.paginator.of(page, size);
    let name:string = this.formSearch.value.name!;
    let favorite= this.formSearch.value.favorite!;
    this.contactService.findAll(name, favorite, this.paginator).subscribe(entity => this.dataSource = entity);
  }

  toogleFavorite = (id: number, position: number) => this.contactService.toogleFavorite(id).subscribe(value => {
    var newValue = !this.dataSource.content?.[position].favorite;
    var traducao = { 0: "removido dos", 1: "adicionado aos" }
    this.messageService.add({ severity: 'success', summary: 'Sucesso', detail: `Contato ${traducao?.[!newValue ? 0 : 1]} favoritos com sucesso!` });
    this.search(0,10);
  });

  openDialog(id: number): void {
    let dialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: {
        title: "Atenção",
        message: "Deseja relamente deletar esse registro?"
      }
    }).afterClosed().subscribe(response => {
      if(response){
        this.contactService.toogleDisable(id).subscribe(value => {
          this.messageService.add({ severity: 'success', summary: 'Sucesso', detail: "Contato excluido com sucesso!" });
          this.search(0,10);
        });
      }
    })
  }

  handlePaginator = (e: PageEvent) => this.search(e.pageIndex, e.pageSize);

}
